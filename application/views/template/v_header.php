 <!DOCTYPE html>
<html>
<head>
	<title>Administrator</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media")?>/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media")?>/assets/css/default.css">
</head>
<body>
	<div class="container-fluid display-table">
		<div class="row display-table-row">
			<!-- side menu -->
			<div class="col-md-2 col-sm-1 hidden-xs display-table-cell valign-top" id="side-menu">
				<h1 class="hidden-xs hidden-sm">Rubik Shop</h1>
				<div class="profil hidden-xs hidden-sm">
					<img src="<?php echo base_url("media")?>/assets/img/logobuku.png" width="100px" class="img-circle">
				</div>
				<!-- navigation content -->
				<ul>
					<li class="<?php if($this->uri->segment(1) == "home") {echo "active-link";}?>">
						<a href="<?php echo site_url("home")?>">
							<span class="glyphicon glyphicon-th"></span>
							<span class="hidden-xs hidden-sm">Home</span>
						</a>
					</li>

					<li class="<?php if($this->uri->segment(1) == "buku") {echo "active-link";}?>">
						<a href="<?php echo site_url("buku")?>">
							<span class="glyphicon glyphicon-book"></span>
							<span class="hidden-xs hidden-sm">Buku</span>
						</a>
					</li>

					<li class="<?php if($this->uri->segment(1) == "distributor") {echo "active-link";}?>">
						<a href="<?php echo site_url("distributor")?>">
							<span class="glyphicon glyphicon-plane"></span>
							<span class="hidden-xs hidden-sm">Distributor</span>
						</a>
					</li>

					<li class="<?php if($this->uri->segment(1) == "pasok") {echo "active-link";}?>">
						<a href="<?php echo site_url("pasok")?>">
							<span class="glyphicon glyphicon-shopping-cart"></span>
							<span class="hidden-xs hidden-sm">Pasok</span>
						</a>
					</li>

					<li class="<?php if($this->uri->segment(1) == "kasir") {echo "active-link";}?>">
						<a href="<?php echo site_url("kasir")?>">
							<span class="glyphicon glyphicon-user"></span>
							<span class="hidden-xs hidden-sm">Kasir</span>
						</a>
					</li>
					<li class="<?php if($this->uri->segment(1) == "penjualan") {echo "active-link";}?>">
						<a href="<?php echo site_url("penjualan")?>">
							<span class="glyphicon glyphicon-usd"></span>
							<span class="hidden-xs hidden-sm">Penjualan</span>
						</a>
					</li>
					<?php
						if($this->session->userdata("akses") == "1")
						{
							?>
								<li class="">
									<a href="<?php echo site_url("backup/backup_db")?>">
										<span class="glyphicon glyphicon-floppy-disk"></span>
										<span class="hidden-xs hidden-sm">Backup database</span>
									</a>
								</li>
							<?php
						}
					?>
				</ul>
			</div>
			<div class="col-md-10 col-sm-11 display-table-cell valign-top">
				<div class="row">
					<header id="nav-header" class="clearfix">
						<div class="col-md-5">
							<!-- nav toggle -->
							<nav class="navbar-default pull-left">
								<button class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu">
									<span class="sr-only">Dede Rusliandi</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</nav>
							<!-- end nav toggle -->
							<!-- <p class="text-example hidden-sm hidden-xs">Selamat Datang di halaman admin</p> -->
						</div>
						<div class="col-md-7">
							<ul class="pull-right">
								<li class="hidden-xs">
									<span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata("nama")?>
								</li>
								<li>
									<a href="<?php echo site_url("login/logout")?>" class="logout">
										<span class="glyphicon glyphicon-log-out"></span>
										Logout
									</a>
								</li>
							</ul>
						</div>
					</header>
				</div>