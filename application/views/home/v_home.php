

<?php
	if($success)
	{
		?>
			<div class="pop_up">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success">
							<?= $success?>
							<button class="close" data-dismiss="alert" aria-label="Close">&times;</button>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
?>


 <div id="dashboard-con">
	<div class="row">
		<div class="col-md-3">
			<div class="dashboard-content">
				<header class="clearfix">
				<label class="label label-default">Data Buku</label>
				</header>
				<table>
					<th><h1><?php echo count($data_buku)?></h1></th>
				</table>
				<div class="clearfix">
					<a href="<?php echo site_url("buku")?>" class="pull-right"><button class="btn btn-info btn-sm"><span class="glyphicon glyphicon-book"></span> Lihat Buku</button></a>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="dashboard-content">
				<header class="clearfix">
				<label class="label label-default">Data Distributor</label>
				</header>
				<table>
					<th><h1><?php echo count($data_distributor)?></h1></th>
				</table>
				<div class="clearfix">
					<a href="<?php echo site_url("distributor")?>" class="pull-right"><button class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plane"></span> Lihat Distributor</button></a>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="dashboard-content">
				<header class="clearfix">
				<label class="label label-default">Data Pasok</label>
				</header>
				<table>
					<th><h1><?php echo count($data_pasok)?></h1></th>
				</table>
				<div class="clearfix">
					<a href="<?php echo site_url("pasok")?>" class="pull-right"><button class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-shopping-cart"></span> Lihat Pasok</button></a>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="dashboard-content">
				<header class="clearfix">
				<label class="label label-default">Data Kasir</label>
				</header>
				<table>
					<th><h1><?php echo count($data_kasir)?></h1></th>
				</table>
				<div class="clearfix">
					<a href="<?php echo site_url("kasir")?>" class="pull-right"><button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-user"></span> Lihat Kasir</button></a>
				</div>
			</div>
		</div>
	</div>
</div>

 <div id="dashboard-con">
	<div class="row">
		<div class="col-md-6">
			<div class="dashboard-content">
				<header class="clearfix">
				<label class="label label-default">Data Distributor</label>
				</header>
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Distributor</th>
							<th>Telepon</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 1;
							foreach($read_distributor as $distributor)
							{
								?>
									<tr>
										<td><?php echo $no++?></td>
										<td><label class="label label-warning"><?php echo $distributor->nama_distributor?></label></td>
										<td><?php echo $distributor->telepon?></td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<div class="clearfix">
					<a href="<?php echo site_url("distributor")?>" class="pull-right"><button class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plane"></span> Lihat Distributor</button></a>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="dashboard-content">
				<header class="clearfix">
				<label class="label label-default">Data Kasir</label>
				</header>
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Kasir</th>
							<th>Telepon</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no =1;
							foreach($read_kasir as $kasir)
							{	
								?>
									<tr>
										<td><?php echo $no++?></td>
										<td><span class="glyphicon glyphicon-user"></span> <?php echo $kasir->nama?></td>
										<td><?php echo $kasir->telepon?></td>
										<td>
											<?php
												if($kasir->akses == "1")
												{
													?>
														<label class="label label-warning">Admin</label>
													<?php
												}
												else
												{
													?>
														<label class="label label-info">Kasir</label>
													<?php
												}
											?>
										</td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<div class="clearfix">
					<a href="<?php echo site_url("kasir")?>" class="pull-right"><button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-user"></span> Lihat Kasir</button></a>
				</div>
			</div>
		</div>
	</div>
</div>


	