<?php
	if(@$form_edit)
	{
		$url 		= "pasok/do_edit/".$result->id_pasok;
		$page_title	= "Edit Data Pasok";
	}
	else
	{
		$url 		= "pasok/do_tambah/";
		$page_title	= "Tambah Data Pasok";
	}
?>

<div id="content">
	<header class="clearfix">
		<a href="<?php echo site_url("pasok")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>
		
		<h5 class="content-title pull-right"><?php echo $page_title?></h5>
	</header>
	<div class="content-inner">
		<form method="POST" class="form-horizontal form-wrapper" action="<?php echo site_url($url)?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Nama Distributor</label>
					</div>
					<div class="col-md-10">
						<select class="form-control" name="id_distributor" required>
							<option value="">[ Pilih Distributor ]</option>
							<?php
								foreach($data_distributor as $distributor)
								{
									?>
										<option value="<?php echo $distributor->id_distributor?>" <?php  echo @$result->id_distributor == $distributor->id_distributor ? "selected" : ""?>><?php echo $distributor->nama_distributor?></option>
									<?php
								}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Judul Buku</label>
					</div>
					<div class="col-md-10">
						<select class="form-control" name="id_buku" required>
							<option value="">[ Pilih Buku ]</option>
							<?php
								foreach($data_buku as $buku)
								{
									?>
										<option value="<?php echo $buku->id_buku?>" <?php echo @$result->id_buku == $buku->id_buku ? "selected" : ""?>><?php echo $buku->judul?></option>
									<?php
								}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Jumlah</label>
					</div>
					<div class="col-md-10">
						<input type="number" name="jumlah" class="form-control" placeholder="Jumlah" value="<?php echo @$result->jumlah?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="clearfix">
					<button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>