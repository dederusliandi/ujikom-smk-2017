<div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
		<div class="col-md-12">
			<header class="clearfix">
				<a href="<?php echo site_url("pasok")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

				<h5 class="pull-right">Detail Pasok</h5>
			</header>

			<table class="table  table-hover">
				<thead>
					<tr>
						<td>Nama Distributor</td>
						<th><?php echo @$data->nama_distributor?></th>
					</tr>
					<tr>
						<td>Judul Buku</td>
						<td><?php echo @$data->judul?></td>
					</tr>
					<tr>
						<td>Jumlah</td>
						<td><?php echo @$data->jumlah?></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	</div>
</div>