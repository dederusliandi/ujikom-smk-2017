<?php
	if(@$form_edit)
	{
		$url 		= "distributor/do_edit/".$result->id_distributor;
		$page_title	= "Edit Data Distributor";
	}
	else
	{
		$url 		= "distributor/do_tambah/";
		$page_title	= "Tambah Data Distributor";
	}
?>


<div id="content">
	<header class="clearfix">
		<a href="<?php echo site_url("distributor")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

		<h5 class="content-title pull-right"><?php echo $page_title?></h5>
	</header>
	<div class="content-inner">
		<form method="POST" class="form-horizontal form-wrapper" action="<?php echo site_url($url)?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Nama Distributor</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="nama_distributor" class="form-control" placeholder="Nama Distributor" value="<?php echo @$result->nama_distributor?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Alamat</label>
					</div>
					<div class="col-md-10">
						<textarea name="alamat" class="form-control" placeholder="Alamat" rows="8" required><?php echo @$result->alamat?></textarea>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Telepon</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="telepon" class="form-control" placeholder="Telepon" value="<?php echo @$result->telepon?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="clearfix">
					<button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>