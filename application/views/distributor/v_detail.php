<div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
		<div class="col-md-12">
			<header class="clearfix">
				<a href="<?php echo site_url("distributor")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

				<h5 class="pull-right">Detail distributor</h5>
			</header>

			<table class="table  table-hover">
				<thead>
					<tr>
						<td>Nama Distributor</td>
						<th><?php echo @$data->nama_distributor?></th>
					</tr>
					<tr>
						<td>Alamat</td>
						<td><?php echo @$data->alamat?></td>
					</tr>
					<tr>
						<td>Telepon</td>
						<td><?php echo @$data->telepon?></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	</div>
</div>