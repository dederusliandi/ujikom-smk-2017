<?php
	if(!empty($success)){
		?>
			 <div class="pop_up">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success">
							<?= @$success?>
							<button class="close" data-dismiss="alert" aria-label="Close">&times;</button>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
?>

<div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
			<div class="col-md-12">
				<header class="clearfix">
					<?php
						if($this->session->userdata("akses") == "1")
						{
							?>
								<a href="<?php echo site_url("distributor/tambah")?>"><button class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Tambah Data</button></a>
							<?php
						}
					?>

					<h5 class="pull-right">Data Distributor</h5>
				</header>

				<div class="pull-right">
					<form class="" method="post" action="<?php echo site_url("distributor")?>">
						<input type="text" name="search" id="search-field" placeholder="cari berdasarkan nama distributor . . . .">
						<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
					</form>
				</div>

				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Distributor</th>
							<th>Alamat</th>
							<th>Telepon</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no =1;
							foreach($data as $row)
							{
								?>
									<tr>
										<td><?php echo $no++?></td>
										<td><?php echo $row->nama_distributor?></td>
										<td>
											<?php
												if(strlen($row->alamat) <= 40)
												{
													echo $row->alamat;
												}
												else
												{
													echo substr($row->alamat, 0,40)."....";
												}
											?>
										</td>
										<td><?php echo $row->telepon?></td>
										<td>
											<a href="<?php echo site_url("distributor/detail/".$row->id_distributor)?>"><button class="btn btn-sm btn-info">Detail</button></a>
											<?php
												if($this->session->userdata("akses") == "1")
												{
													?>
														<a href="<?php echo site_url("distributor/edit/".$row->id_distributor)?>"><button class="btn btn-sm btn-warning">Edit</button></a>
														<a href="<?php echo site_url("distributor/delete/".$row->id_distributor)?>"><button class="btn btn-sm btn-danger">Hapus</button></a>
													<?php
												}
											?>


										</td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<div class="clearfix">
					<ul class="pagination pull-right">
						<?php echo @$this->pagination->create_links()?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>