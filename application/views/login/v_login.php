<!DOCTYPE html>
<html>
<head>
	<title><?php echo "Administrator | Sign In"?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media")?>/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("media")?>/assets/css/login.css">
</head>
<body>
	<div id="login">
		<div class="content">
			<div class="form-content">
				<?php
					if(!empty($error))
					{
						?>
							<div class="alert alert-danger">
								<?php echo $error?>
								<button class="close" data-dismiss="alert" aria-label="Close">&times;</button>
							</div>
						<?php
					}
				?>
				<form class="form-horizontal" method="POST" action="<?php echo site_url("login/do_login")?>">
				<h2>Login - Kasir</h2>
				<!-- <div class="logo">	
					<img src="<?php echo base_url("media")?>/assets/img/logobuku.png" width="120px" class="img-circle">
				</div> -->
				<div class="form-group has-feedback">
					<input type="text" name="username" class="form-control" placeholder="Username">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control" placeholder="Password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>

				<div class="form-group">
					<input type="submit" name="submit" value="Sign in" class="btn btn-info btn-block">
				</div>
			</form>
			</div>
		</div>
	</div>
</body>
</html>