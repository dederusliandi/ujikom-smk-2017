<div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
		<div class="col-md-12">
			<header class="clearfix">
				<a href="<?php echo site_url("penjualan")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

				<h5 class="pull-right">Detail Pasok</h5>
			</header>

			<table class="table  table-hover">
				<thead>
					<tr>
						<td>Judul Buku</td>
						<th><?php echo @$data->judul?></th>
					</tr>
					<tr>
						<td>Penulis</td>
						<td><?php echo @$data->penulis?></td>
					</tr>
					<tr>
						<td>Penerbit</td>
						<td><?php echo @$data->penerbit?></td>
					</tr>
					<tr>
						<td>Tahun</td>
						<td><?php echo @$data->tahun?></td>
					</tr>
					<tr>
						<td>Harga</td>
						<td><?php echo "Rp. ".@$data->harga_jual?></td>
					</tr>
					<tr>
						<td>Diskon</td>
						<td><?php echo @$data->diskon?></td>
					</tr>
					<tr>
						<td>PPN</td>
						<td><?php echo @$data->ppn?></td>
					</tr>
					<tr>
						<td>Jumlah Pembelian</td>
						<td><?php echo @$data->jumlah?></td>
					</tr>
					<tr>
						<td>Total Pembayaran</td>
						<td><?php echo "Rp. ". @$data->total?></td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	</div>
</div>