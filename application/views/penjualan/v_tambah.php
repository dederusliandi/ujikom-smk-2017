 <div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
			<div class="col-md-12">
				<header class="clearfix">
					<a href="<?php echo site_url("penjualan")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

					<h5 class="pull-right">Data Penjualan</h5>
				</header>

				<div class="pull-right">
					<form class="" method="post" action="<?php echo site_url("penjualan/tambah")?>">
						<input type="text" name="search" id="search-field" placeholder="Cari berdasarkan judul buku . . . .">
						<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
					</form>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul Buku</th>
							<th>Penulis</th>
							<th>Harga</th>
							<th>Stok</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no =1;
							foreach($data as $row)
							{
								?>
									<tr>
										<td><?php echo $no++?></td>
										<td>
											<?php
												if(strlen($row->judul) <= 40)
												{
													echo $row->judul;
												}
												else
												{
													echo substr($row->judul, 0,40)."....";
												}
											?>
										</td>
										<td><?php echo $row->penulis?></td>
										<td><?php echo "Rp. ".$row->harga_jual?></td>
										<td><?php echo $row->stok?></td>
										<td>
											<?php
												if($row->stok == "0")
												{
													?>
														<label class="label label-warning">Data Kosong</label>
													<?php
												}
												else
												{
													?>
														<a href="<?php echo site_url("penjualan/beli/".$row->id_buku)?>"><button class="btn btn-sm btn-success"><span class="glyphicon glyphicon-shopping-cart"></span> Beli</button></a>
													<?php
												}
											?>
										</td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<div class="clearfix">
					<ul class="pagination pull-right">
						<?php echo @$this->pagination->create_links()?>
					</ul>
				</div>
			</div>
	</div>
	</div>
</div>