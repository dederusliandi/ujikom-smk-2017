<?php
	if(!empty($error)){
		?>
			 <div class="pop_up">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-danger">
							<?= @$error?>
							<button class="close" data-dismiss="alert" aria-label="Close">&times;</button>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
?>

<div id="content">
	<header class="clearfix">
		<a href="<?php echo site_url("penjualan/tambah/")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

		<h5 class="content-title pull-right">tambah penjualan</h5>
	</header>
	<div class="content-inner">
		<form method="POST" class="form-horizontal form-wrapper" action="<?php echo site_url("penjualan/do_beli/".@$data->id_buku)?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Judul</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="judul" class="form-control" placeholder="Judul" value="<?php echo @$data->judul?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Penulis</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="penulis" class="form-control" placeholder="Penulis" value="<?php echo @$data->penulis?>" readonly>
					</div>
				</div>
			</div>



			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Penerbit</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="telepon" class="form-control" placeholder="Penerbit" value="<?php echo @$data->penerbit?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Tahun</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="tahun" class="form-control" placeholder="Tahun" value="<?php echo @$data->tahun?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Harga</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="harga_jual" class="form-control" placeholder="Harga" value="<?php echo @$data->harga_jual?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Diskon (%)</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="diskon" class="form-control" placeholder="Diskon" value="<?php echo @$data->diskon?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>PPN (%)</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="ppn" class="form-control" placeholder="PPN" value="<?php echo @$data->ppn?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Stok</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="stok" class="form-control" placeholder="stok" value="<?php echo @$data->stok?>" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Jumlah Pembelian</label>
					</div>
					<div class="col-md-10">
						<input type="number" name="jumlah" class="form-control" placeholder="Jumlah Pembelian" min="1" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="clearfix">
					<button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>