 <div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
			<div class="col-md-12">
				<header class="clearfix">
					<a href="<?php echo site_url("penjualan/tambah")?>"><button class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Tambah Penjualan</button></a>

					<h5 class="pull-right">Data Penjualan</h5>
				</header>

				<div class="pull-right">
					<form class="" method="post" action="<?php echo site_url("penjualan")?>">
						<input type="text" name="search" id="search-field" placeholder="Cari berdasarkan judul buku . . . .">
						<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
					</form>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul Buku</th>
							<th>Harga</th>
							<th>Jumlah</th>
							<th>Total</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no =1;
							foreach($data as $row)
							{
								?>
									<tr>
										<td><?php echo $no++?></td>
										<td><?php echo $row->judul?></td>
										<td><?php echo "Rp. ".$row->harga_jual?></td>
										<td><?php echo $row->jumlah?></td>
										<td><?php echo "Rp. ".$row->total?></td>
										<td>
											<a href="<?php echo site_url("penjualan/detail/").$row->id_penjualan?>"><button class="btn btn-sm btn-info">Detail</button></a>
											<?php
												if($this->session->userdata("akses") == "1")
												{
													?>
														<a href="<?php echo site_url("penjualan/delete/").$row->id_penjualan?>"><button class="btn btn-sm btn-danger">Hapus</button></a>
													<?php
												}
											?>
											<a href="<?php echo site_url("laporan/pdf/").$row->id_penjualan?>" target="blank"><button class="btn btn-sm btn-success"><span class="glyphicon glyphicon-file"></span>PDF</button></a>
										</td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>
				<div class="clearfix">
					<ul class="pagination pull-right">
						<?php
							echo @$this->pagination->create_links();
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>