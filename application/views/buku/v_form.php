<?php
	if(@$form_edit)
	{
		$url 		= "buku/do_edit/".$result->id_buku;
		$page_title	= "Edit Data Buku";
	}
	else
	{
		$url 		= "buku/do_tambah/";
		$page_title	= "Tambah Data Buku";
	}
?>


<div id="content">
	<header class="clearfix">
		<a href="<?php echo site_url("buku")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

		<h5 class="content-title pull-right"><?php echo $page_title?></h5>
	</header>
	<div class="content-inner">
		<form action="<?php echo site_url($url)?>" class="form-horizontal form-wrapper" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Judul Buku</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="judul" class="form-control" placeholder="judul" value="<?php echo @$result->judul?>" required>
					</div>
					<div class="col-md-2">
						<label>NO ISBN</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="no_isbn" class="form-control" placeholder="No ISBN" value="<?php echo @$result->no_isbn?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Penulis</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="penulis" class="form-control" placeholder="Penulis" value="<?php echo @$result->penulis?>" required>
					</div>
					<div class="col-md-2">
						<label>Penerbit</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="penerbit" class="form-control" placeholder="Penerbit" value="<?php echo @$result->penerbit?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Tahun</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="tahun" class="form-control" placeholder="Tahun" value="<?php echo @$result->tahun?>" required>
					</div>
					<div class="col-md-2">
						<label>Stok</label>
					</div>
					<div class="col-md-4">
						<input type="number" name="stok" class="form-control" placeholder="Stok" value="<?php echo @$result->stok?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Harga Pokok</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="harga_pokok" class="form-control" placeholder="Harga Pokok" value="<?php echo @$result->harga_pokok?>" required>
					</div>
					<div class="col-md-2">
						<label>Harga Jual</label>
					</div>
					<div class="col-md-4">
						<input type="text" name="harga_jual" class="form-control" placeholder="Harga Jual" value="<?php echo @$result->harga_jual?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>PPN (%)</label>
					</div>
					<div class="col-md-4">
						<input type="number" name="ppn" class="form-control" placeholder="PPN" value="<?php echo @$result->ppn?>" required>
					</div>
					<div class="col-md-2">
						<label>Diskon (%)</label>
					</div>
					<div class="col-md-4">
						<input type="number" name="diskon" class="form-control" placeholder="diskon" value="<?php echo @$result->diskon?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="clearfix">
					<button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>