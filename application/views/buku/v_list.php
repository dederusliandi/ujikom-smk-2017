<?php
	if(!empty($success)){
		?>
			 <div class="pop_up">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success">
							<?= @$success?>
							<button class="close" data-dismiss="alert" aria-label="Close">&times;</button>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
?>

<div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
			<div class="col-md-12">
				<header class="clearfix">
					<?php
						if($this->session->userdata("akses") == "1")
						{
							?>
								<a href="<?php echo site_url("buku/tambah")?>"><button class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Tambah Data</button></a>
							<?php
						}
					?>

					<h5 class="pull-right">Data Buku</h5>
				</header>

				<div class="pull-right">
					<form class="" method="post" action="<?php echo site_url("buku")?>">
						<input type="text" name="search" id="search-field" placeholder="cari berdasarkan judul . . . .">
						<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
					</form>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul</th>
							<th>Penulis</th>
							<th>Penerbit</th>
							<th>Harga</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$no =1;
							foreach($data->result() as $row)
							{
								?>
									<tr>
										<td><?php echo $no++?></td>
										<td>
											<?php
												if(strlen($row->judul) <= 40)
												{
													echo $row->judul;
												}
												else
												{
													echo substr($row->judul, 0,40)."....";
												}
											?>
										</td>
										<td><?php echo $row->penulis?></td>
										<td><?php echo $row->penerbit?></td>
										<td><?php echo "Rp. ".$row->harga_jual?></td>
										<td>
											<a href="<?php echo site_url("buku/detail/".$row->id_buku)?>"><button class="btn btn-sm btn-info">Detail</button></a>
											<?php
												if($this->session->userdata("akses") == "1")
												{
													?>
														<a href="<?php echo site_url("buku/edit/".$row->id_buku)?>"><button class="btn btn-sm btn-warning">Edit</button></a>
														<a href="<?php echo site_url("buku/delete/".$row->id_buku)?>"><button class="btn btn-sm btn-danger">Hapus</button></a>
													<?php
												}
											?>
										</td>
									</tr>
								<?php
							}
						?>
					</tbody>
				</table>

				<div class="clearfix">
					<ul class="pagination pull-right">
						<?php echo @$this->pagination->create_links()?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
