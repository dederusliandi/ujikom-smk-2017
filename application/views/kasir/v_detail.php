<div id="dashboard-con">
	<div class="dashboard-content">
		<div class="row">
		<div class="col-md-12">
			<header class="clearfix">
				<a href="<?php echo site_url("kasir")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

				<h5 class="pull-right">Detail Kasir</h5>
			</header>

			<table class="table  table-hover">
				<thead>
					<tr>
						<td>Nama Kasir</td>
						<th><?php echo @$data->nama?></th>
					</tr>
					<tr>
						<td>Alamat</td>
						<td><?php echo @$data->alamat?></td>
					</tr>
					<tr>
						<td>Telepon</td>
						<td><?php echo @$data->telepon?></td>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							<?php
								if($data->status ="1")
								{
									?>
										<label class="label label-success">Active</label>
									<?php
								}
								else
								{
									?>
										<label class="label label-success">Non Active</label>
									<?php
								}
							?>
						</td>
					</tr>
					<tr>
						<td>Hak Akses</td>
						<td>
							<?php
								if($data->akses ="1")
								{
									?>
										<label class="label label-warning">Admin</label>
									<?php
								}
								else
								{
									?>
										<label class="label label-info">Kasir</label>
									<?php
								}
							?>

						</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	</div>
</div>