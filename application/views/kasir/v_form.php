<?php
	if(@$form_edit)
	{
		$url 		= "kasir/do_edit/".$result->id_kasir;
		$page_title	= "Edit Data Kasir";
	}
	else
	{
		$url 		= "kasir/do_tambah/";
		$page_title	= "Tambah Data Kasir";
	}
?>

<div id="content">
	<header class="clearfix">
		<a href="<?php echo site_url("kasir")?>"><button class="btn btn-success"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</button></a>

		<h5 class="content-title pull-right"><?php echo $page_title?></h5>
	</header>
	<div class="content-inner">
		<form method="POST" class="form-horizontal form-wrapper" action="<?php echo site_url($url)?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Nama</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="nama" class="form-control" placeholder="Nama Distributor" value="<?php echo @$result->nama?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Alamat</label>
					</div>
					<div class="col-md-10">
						<textarea name="alamat" class="form-control" placeholder="Alamat" rows="8" required><?php echo @$result->alamat?></textarea>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Telepon</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="telepon" class="form-control" placeholder="Telepon" value="<?php echo @$result->telepon?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Status</label>
					</div>
					<div class="col-md-10">
						<select class="form-control" name="status" required>
							<option value="">[ Pilih Status ]</option>
							<option value="1" <?php echo @$result->status == "1" ? "selected" : ""?>>Active</option>
							<option value="0" <?php echo @$result->status == "0" ? "selected" : ""?>>Non Active</option>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Username</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo @$result->username?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Password</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="password" class="form-control" placeholder="Password" value="<?php echo @$result->password?>" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Hak Akses</label>
					</div>
					<div class="col-md-10">
						<select class="form-control" name="akses" required>
							<option value="">[ Pilih Hak Akses ]</option>
							<option value="1" <?php echo @$result->akses == "1" ? "selected" : ""?>>Administrator</option>
							<option value="0" <?php echo @$result->akses == "0" ? "selected" : ""?>>Kasir</option>	
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="clearfix">
					<button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-save"></span> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>