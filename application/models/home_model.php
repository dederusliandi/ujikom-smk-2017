<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home_model extends CI_Model {

	function create($data)
	{
		$this->db->insert("t_distributor",$data);
	}

	function readBuku($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_buku");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function readDistributor($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_distributor");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function readKasir($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_kasir");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function readPasok($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_pasok");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}
}
