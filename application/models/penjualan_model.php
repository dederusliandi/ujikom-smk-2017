<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan_model extends CI_Model {

	function create($data)
	{
		$this->db->insert("t_penjualan",$data);
	}

	function read($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$this->db->join("t_kasir","t_penjualan.id_kasir = t_kasir.id_kasir");
		$this->db->join("t_buku","t_buku.id_buku = t_penjualan.id_buku");

		$query	= $this->db->get("t_penjualan");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}


	function readBuku($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_buku");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function delete($id)
	{
		$this->db->where($id);
		$this->db->delete("t_penjualan");
	}
}
