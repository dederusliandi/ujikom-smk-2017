<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pasok_model extends CI_Model {

	function create($data)
	{
		$this->db->insert("t_pasok",$data);
	}

	function read($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$this->db->join("t_distributor","t_distributor.id_distributor = t_pasok.id_distributor");
		$this->db->join("t_buku","t_buku.id_buku = t_pasok.id_buku");

		$query	= $this->db->get("t_pasok");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function readDistributor($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_distributor");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function readBuku($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_buku");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function update($id,$data)
	{
		$this->db->where($id);
		$this->db->update("t_pasok",$data);	
	}

	function delete($id)
	{
		$this->db->where($id);
		$this->db->delete("t_pasok");
	}
}
