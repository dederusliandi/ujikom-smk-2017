<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kasir_model extends CI_Model {

	function create($data)
	{
		$this->db->insert("t_kasir",$data);
	}

	function read($where="",$order="")
	{
		if(!empty($where)) $this->db->where($where);
		if(!empty($order)) $this->db->where($order);

		$query	= $this->db->get("t_kasir");

		if($query AND $query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function update($id,$data)
	{
		$this->db->where($id);
		$this->db->update("t_kasir",$data);	
	}

	function delete($id)
	{
		$this->db->where($id);
		$this->db->delete("t_kasir");
	}
}
