<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penjualan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$id_kasir	= $this->session->userdata("id_kasir");
		if(empty($id_kasir)) redirect("login");

		$this->load->model("penjualan_model");
	}

	function index()
	{
		$data['success']	= $this->session->flashdata("success");
		$data['error']		= $this->session->flashdata("error");

		$search = @$_POST['search'];

		if(!empty($search))
		{
			$this->db->join("t_kasir","t_penjualan.id_kasir = t_kasir.id_kasir");
			$this->db->join("t_buku","t_buku.id_buku = t_penjualan.id_buku");

			$this->db->where("t_buku.judul LIKE '%$search%'");
			$result	= $this->db->get("t_penjualan");

		}
		else
		{
			$this->load->library("pagination");
			$config['base_url']		= "http://localhost/ujikom_dede/index.php/penjualan/index";
			$config['per_page']		= 10;
			$config['num_links']	= 1;
			$config['total_rows']	= $this->db->get("t_penjualan")->num_rows();

			$config['first_tag_open']				= '<li>';
			$config['last_tag_open']				= '<li>';

			$config['first_tag_close']				= '</li>';
			$config['last_tag_close']				= '</li>';

			
			$config['next_tag_open']				= '<li>';
			$config['prev_tag_open']				= '<li>';

			$config['next_tag_close']				= '</li>';
			$config['prev_tag_close']				= '</li>';

			$config['num_tag_open']					= '<li>';
			$config['num_tag_close']				= '</li>';

			$config['cur_tag_open']				= "<li class=\"active\"><span><b>";
			$config['cur_tag_close']				= "</b></span></li>";

			$this->pagination->initialize($config);
			
			$this->db->join("t_kasir","t_penjualan.id_kasir = t_kasir.id_kasir");
			$this->db->join("t_buku","t_buku.id_buku = t_penjualan.id_buku");

			$result = $this->db->get("t_penjualan",$config['per_page'],$this->uri->segment(3));
		}

		
		$data['data']	= $result->result();
		
		$data['view']	= "penjualan/v_list";
		$this->load->view("index",$data);
	}

	function tambah()
	{
		$search = @$_POST['search'];

		if(!empty($search))
		{
			$this->db->where("judul LIKE '%$search%'");
			$result	= $this->db->get("t_buku");

		}
		else
		{
			$this->load->library("pagination");
			$config['base_url']		= "http://localhost/ujikom_dede/index.php/penjualan/tambah";
			$config['per_page']		= 10;
			$config['num_links']	= 1;
			$config['total_rows']	= $this->db->get("t_buku")->num_rows();

			$config['first_tag_open']				= '<li>';
			$config['last_tag_open']				= '<li>';

			$config['first_tag_close']				= '</li>';
			$config['last_tag_close']				= '</li>';

			
			$config['next_tag_open']				= '<li>';
			$config['prev_tag_open']				= '<li>';

			$config['next_tag_close']				= '</li>';
			$config['prev_tag_close']				= '</li>';

			$config['num_tag_open']					= '<li>';
			$config['num_tag_close']				= '</li>';

			$config['cur_tag_open']				= "<li class=\"active\"><span><b>";
			$config['cur_tag_close']				= "</b></span></li>";

			$this->pagination->initialize($config);

			$result = $this->db->get("t_buku",$config['per_page'],$this->uri->segment(3));
		}

		
		$data['data']	= $result->result();

		// $data['data']	= $this->buku_model->read();
		$data['view']	= "penjualan/v_tambah";
		$this->load->view("index",$data);
	}

	function beli($id)
	{
		$data['success']	= $this->session->flashdata("success");
		$data['error']		= $this->session->flashdata("error");

		$result			= $this->penjualan_model->readBuku("id_buku = '$id'");
		$data['data']	= $result[0];
		$data['view']	= "penjualan/v_form_tambah";
		$this->load->view("index",$data);
	}

	function do_beli($id)
	{
		$this->load->model("buku_model");


		$stok				= $this->buku_model->read("id_buku ='$id'");

		$harga_jual 		= $_POST['harga_jual'];
		$diskon 			= $_POST['diskon'];
		$ppn				= $_POST['ppn'];
		$jumlah_beli		= $_POST['jumlah'];

		$decimal_diskon		= $diskon * 0.01;
		$decimal_ppn		= $ppn * 0.01;
		$harga_diskon		= $harga_jual * $decimal_diskon;
		$harga_ppn			= $harga_jual * $decimal_ppn; 
		$harga_beli			= ($harga_jual - $harga_diskon) + $harga_ppn;

		if($stok[0]->stok < $jumlah_beli)
		{

			$data['error']	= $this->session->set_flashdata("error","stok tidak mencukupi untuk melakukan proses pembelian");

		redirect("penjualan/beli/".$id);



		}
		else
		{
			$total_dibayar		= $harga_beli * $jumlah_beli;

			$data	= array(
				"id_buku"	=> $id,
				"id_kasir"	=> $this->session->userdata("id_kasir"),
				"jumlah"	=> $jumlah_beli,
				"total"		=> $total_dibayar,
				"tanggal"	=> date('Y-m-d')
				);

			$this->penjualan_model->create($data);
			$this->session->set_flashdata("success","proses pembelian berhasil");

			redirect("penjualan");
		}
	}

	function detail($id)
	{
		$result	= $this->penjualan_model->read("id_penjualan = '$id'");

		$data['data']	= $result[0];
		$data['view']	= "penjualan/v_detail";

		$this->load->view("index",$data);
	}

	function delete($id)
	{
		$this->penjualan_model->delete("id_penjualan = '$id'");
		$this->session->set_flashdata("success","hapus data pembelian berhasil");
		redirect("penjualan");
	}


}
