<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$id_kasir	= $this->session->userdata("id_kasir");
		if(empty($id_kasir)) redirect("login");

		$this->load->library("fpdf");

		$this->load->model("laporan_model");
	}

	function pdf($id)
	{
		$result = $this->laporan_model->read("t_penjualan.id_penjualan = '$id'");
		$data	= @$result[0];

		$pdf = NEW FPDF('P','mm','A5');
		$pdf->addPage();
		$pdf->Image(base_url("media/")."assets/img/logobuku.png",6,2,25);
		$pdf->setTitle("Struk pembayaran ".$data->judul);

		$pdf->setFont("arial",'b','14');
		$pdf->cell(0,5,'Rubik Shop',0,1,'C');
		$pdf->Ln(1);

		$pdf->setFont("arial",'','9');
		$pdf->cell(0,5,'Jl Telekomunikasi 1 Telkom University',0,1,'C');
		$pdf->Ln(2);
		$pdf->cell(130,0.4,'',0,1,'c',true);
		$pdf->Ln(1);
		$pdf->cell(130,0.1,'',0,1,'c',true);
		$pdf->Ln(3);
		$pdf->setFont("arial",'','10');
		$pdf->cell(0,5,'Struk Pembayaran Buku',0,1,'C');
		$pdf->Ln(3);


		$pdf->setFont("arial",'','8');

		$pdf->cell(30,8,'Judul Buku',1,0,'C');
		$pdf->cell(100,8,$data->judul,1,0,'L');
		
		$pdf->Ln(8);
		$pdf->cell(30,8,'Penulis',1,0,'C');
		$pdf->cell(100,8,$data->penulis,1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Penerbit',1,0,'C');
		$pdf->cell(100,8,$data->penerbit,1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Tahun',1,0,'C');
		$pdf->cell(100,8,$data->tahun,1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Harga',1,0,'C');
		$pdf->cell(100,8,"Rp. ".$data->harga_jual,1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Diskon',1,0,'C');
		$pdf->cell(100,8,$data->diskon." %",1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'PPN',1,0,'C');
		$pdf->cell(100,8,$data->ppn." %",1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Jumlah Pembelian',1,0,'C');
		$pdf->cell(100,8,$data->jumlah,1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Total Pembayaran',1,0,'C');
		$pdf->cell(100,8,"Rp. ".$data->total,1,0,'L');

		$pdf->Ln(8);
		$pdf->cell(30,8,'Kasir',1,0,'C');
		$pdf->cell(100,8,ucwords($data->nama),1,0,'L');


		$pdf->output();
	}

}
