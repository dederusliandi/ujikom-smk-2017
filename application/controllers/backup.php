<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class backup extends CI_Controller {
	
	function backup_db()
	{
		$this->load->dbutil();

		$data = array(
			"filename" => 'db_toko_buku_backup.sql',
			"format"	=> "zip"
			);

		

		$backup =& $this->dbutil->backup($data);
		$nama_db = 'db_backup_toko_buku'.'.zip';
		$simpan	= '/backup'.$nama_db;

		$this->load->helper("file");
		write_file($simpan,$backup);

		$this->load->helper("download");
		force_download($nama_db,$backup);
		}


}
