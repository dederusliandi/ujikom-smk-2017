<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	function index()
	{
		$data["error"]		= $this->session->flashdata("error");
		$data["success"]	= $this->session->flashdata("success");
		$this->load->view("login/v_login",$data);
	}

	function do_login()
	{
		$this->load->model("login_model");

		$data	= array(
				"username"	=> $_POST['username'],
				"password"	=> $_POST['password'],
				"status"	=> "1"
			);

		$result	= $this->login_model->read($data);

		if(count($result) != 0)
		{
			$id_kasir	= $result[0]->id_kasir;
			$this->session->set_userdata("id_kasir",$id_kasir);

			$nama	= $result[0]->nama;
			$this->session->set_userdata("nama",$nama);

			$akses	= $result[0]->akses;
			$this->session->set_userdata("akses",$akses);


			$this->session->set_flashdata("success","selamat datang "."<b>".$this->session->userdata("nama")."</b>"." di halaman administrator");
			redirect("home");
		}
		else
		{
			$this->session->set_flashdata("error","Login gagal");
			redirect("login");
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect("login");
	}

}
