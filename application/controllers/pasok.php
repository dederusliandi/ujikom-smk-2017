<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pasok extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$id_kasir	= $this->session->userdata("id_kasir");
		if(empty($id_kasir)) redirect("login");

		$this->load->model("pasok_model");
	}

	function index()
	{
		$data['success']	= $this->session->flashdata("success");
		$data['error']		= $this->session->flashdata("error");

		$search = @$_POST['search'];

		if(!empty($search))
		{
			$this->db->join("t_distributor","t_distributor.id_distributor = t_pasok.id_distributor");
			$this->db->join("t_buku","t_buku.id_buku = t_pasok.id_buku");
			$this->db->where("t_distributor.nama_distributor LIKE '%$search%'");
			$result	= $this->db->get("t_pasok");

		}
		else
		{
			$this->load->library("pagination");
			$config['base_url']		= "http://localhost/ujikom_dede/index.php/pasok/index";
			$config['per_page']		= 10;
			$config['num_links']	= 1;
			$config['total_rows']	= $this->db->get("t_pasok")->num_rows();

			$config['first_tag_open']				= '<li>';
			$config['last_tag_open']				= '<li>';

			$config['first_tag_close']				= '</li>';
			$config['last_tag_close']				= '</li>';

			
			$config['next_tag_open']				= '<li>';
			$config['prev_tag_open']				= '<li>';

			$config['next_tag_close']				= '</li>';
			$config['prev_tag_close']				= '</li>';

			$config['num_tag_open']					= '<li>';
			$config['num_tag_close']				= '</li>';

			$config['cur_tag_open']				= "<li class=\"active\"><span><b>";
			$config['cur_tag_close']				= "</b></span></li>";

			$this->pagination->initialize($config);
			
			$this->db->join("t_distributor","t_distributor.id_distributor = t_pasok.id_distributor");
			$this->db->join("t_buku","t_buku.id_buku = t_pasok.id_buku");
			$result = $this->db->get("t_pasok",$config['per_page'],$this->uri->segment(3));
		}

		
		$data['data']	= $result->result();
		
		$data['view']	= "pasok/v_list";
		$this->load->view("index",$data);
	}

	function detail($id)
	{
		$result	= $this->pasok_model->read("id_pasok = '$id'");

		$data['data']	= $result[0];
		$data['view']	= "pasok/v_detail";

		$this->load->view("index",$data);
	}

	function tambah()
	{
		$data['data_distributor']	= $this->pasok_model->readDistributor();
		$data['data_buku']			= $this->pasok_model->readBuku();
		$data['view']				= "pasok/v_form";

		$this->load->view("index",$data);
	}

	function do_tambah()
	{

		$post	= array(
				"id_distributor"	=> $_POST['id_distributor'],
				"id_buku"			=> $_POST['id_buku'],
				"jumlah"			=> $_POST['jumlah'],
				"tanggal"			=> date("Y-m-d")
			);


		$this->pasok_model->create($post);

		$this->session->set_flashdata("success","tambah data pasok berhasil");

		redirect("pasok");
	}

	function edit($id)
	{
		$result						= $this->pasok_model->read("id_pasok = '$id'");
		$data['data_distributor']	= $this->pasok_model->readDistributor();
		$data['data_buku']			= $this->pasok_model->readBuku();
		$data['result']		= $result[0];
		$data['form_edit']	= TRUE;
		$data['view']		= "pasok/v_form";

		$this->load->view("index",$data);
	}

	function do_edit($id)
	{
		$post	= array(
			"id_distributor"	=> $_POST['id_distributor'],
				"id_buku"			=> $_POST['id_buku'],
				"jumlah"			=> $_POST['jumlah'],
			);
		$this->pasok_model->update("id_pasok = '$id'",$post);
		$this->session->set_flashdata("success","update data pasok berhasil");

		redirect("pasok");
	}

	

	function delete($id)
	{
		$this->pasok_model->delete("id_pasok = '$id'");
		$this->session->set_flashdata("success","hapus data pasok berhasil");
		redirect("pasok");
	}
}
