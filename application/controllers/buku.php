<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class buku extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$id_kasir	= $this->session->userdata("id_kasir");
		if(empty($id_kasir)) redirect("login");

		$this->load->model("buku_model");
	}

	function index()
	{
		$data['success']	= $this->session->flashdata("success");
		$data['error']		= $this->session->flashdata("error");

		$search = @$_POST['search'];

		if(!empty($search))
		{
			$this->db->where("judul LIKE '%$search%'");
			$result	= $this->db->get("t_buku");

		}
		else
		{
			$this->load->library("pagination");
			$config['base_url']		= "http://localhost/ujikom_dede/index.php/buku/index";
			$config['per_page']		= 10;
			$config['num_links']	= 1;
			$config['total_rows']	= $this->db->get("t_buku")->num_rows();

			$config['first_tag_open']				= '<li>';
			$config['last_tag_open']				= '<li>';

			$config['first_tag_close']				= '</li>';
			$config['last_tag_close']				= '</li>';

			
			$config['next_tag_open']				= '<li>';
			$config['prev_tag_open']				= '<li>';

			$config['next_tag_close']				= '</li>';
			$config['prev_tag_close']				= '</li>';

			$config['num_tag_open']					= '<li>';
			$config['num_tag_close']				= '</li>';

			$config['cur_tag_open']				= "<li class=\"active\"><span><b>";
			$config['cur_tag_close']				= "</b></span></li>";

			$this->pagination->initialize($config);

			$result = $this->db->get("t_buku",$config['per_page'],$this->uri->segment(3));
		}

		
		$data['data']	= $result;

		// $data['data']	= $this->buku_model->read();
		$data['view']	= "buku/v_list";
		$this->load->view("index",$data);
	}

	function detail($id)
	{
		$result	= $this->buku_model->read("id_buku = '$id'");

		$data['data']	= $result[0];
		$data['view']	= "buku/v_detail";

		$this->load->view("index",$data);

	}

	function tambah()
	{
		$data['view']	= "buku/v_form";
		$this->load->view("index",$data);
	}

	function do_tambah()
	{
		$post	= $this->input->post(NULL,TRUE);
		$this->buku_model->create($post);

		$this->session->set_flashdata("success","tambah data buku berhasil");

		redirect("buku");
	}

	function edit($id)
	{
		$result				= $this->buku_model->read("id_buku = '$id'");
		$data['result']		= $result[0];
		$data['form_edit']	= TRUE;
		$data['view']		= "buku/v_form";

		$this->load->view("index",$data);
	}

	function do_edit($id)
	{
		$post	= $this->input->post(NULL,TRUE);
		$this->buku_model->update("id_buku = '$id'",$post);
		$this->session->set_flashdata("success","Update data buku berhasil");

		redirect("buku");
	}

	

	function delete($id)
	{
		$this->buku_model->delete("id_buku = '$id'");
		$this->session->set_flashdata("success","Hapus data buku berhasil");
		
		redirect("buku");
	}
}
