<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kasir extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$id_kasir	= $this->session->userdata("id_kasir");
		if(empty($id_kasir)) redirect("login");

		$this->load->model("kasir_model");
	}

	function index()
	{
		$data['success']	= $this->session->flashdata("success");
		$data['error']		= $this->session->flashdata("error");

		$search = @$_POST['search'];

		if(!empty($search))
		{
			$this->db->where("nama LIKE '%$search%'");
			$result	= $this->db->get("t_kasir");

		}
		else
		{
			$this->load->library("pagination");
			$config['base_url']		= "http://localhost/ujikom_dede/index.php/kasir/index";
			$config['per_page']		= 10;
			$config['num_links']	= 1;
			$config['total_rows']	= $this->db->get("t_kasir")->num_rows();

			$config['first_tag_open']				= '<li>';
			$config['last_tag_open']				= '<li>';

			$config['first_tag_close']				= '</li>';
			$config['last_tag_close']				= '</li>';

			
			$config['next_tag_open']				= '<li>';
			$config['prev_tag_open']				= '<li>';

			$config['next_tag_close']				= '</li>';
			$config['prev_tag_close']				= '</li>';

			$config['num_tag_open']					= '<li>';
			$config['num_tag_close']				= '</li>';

			$config['cur_tag_open']				= "<li class=\"active\"><span><b>";
			$config['cur_tag_close']				= "</b></span></li>";

			$this->pagination->initialize($config);

			$result = $this->db->get("t_kasir",$config['per_page'],$this->uri->segment(3));
		}

		
		$data['data']	= $result->result();

		// $data['data']	= $this->buku_model->read();
		$data['view']	= "kasir/v_list";
		$this->load->view("index",$data);
	}

	function detail($id)
	{
		$result	= $this->kasir_model->read("id_kasir = '$id'");

		$data['data']	= $result[0];
		$data['view']	= "kasir/v_detail";

		$this->load->view("index",$data);
	}


	function tambah()
	{
		$data['view']	= "kasir/v_form";
		$this->load->view("index",$data);
	}

	function do_tambah()
	{
		$post	= $this->input->post(NULL,TRUE);
		$this->kasir_model->create($post);

		$this->session->set_flashdata("success","tambah data kasir berhasil");

		redirect("kasir");
	}

	function edit($id)
	{
		$result				= $this->kasir_model->read("id_kasir = '$id'");
		$data['result']		= $result[0];
		$data['form_edit']	= TRUE;
		$data['view']		= "kasir/v_form";

		$this->load->view("index",$data);
	}

	function do_edit($id)
	{
		$post	= $this->input->post(NULL,TRUE);
		$this->kasir_model->update("id_kasir = '$id'",$post);

		$this->session->set_flashdata("success","update data kasir berhasil");

		redirect("kasir");
	}

	

	function delete($id)
	{
		$this->kasir_model->delete("id_kasir = '$id'");
		$this->session->set_flashdata("success","hapus data kasir berhasil");
		redirect("kasir");
	}
}
