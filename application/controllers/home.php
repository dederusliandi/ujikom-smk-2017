<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	// extends library yang ada codeigniter
	// berfungsi untuk menjalankan fungsi diseluruh fungsi
	function __construct()
	{
		parent::__construct();

		$id_kasir	= $this->session->userdata("id_kasir");
		if(empty($id_kasir)) redirect("login");

		$this->load->model("home_model");
	}

	function index()
	{
		// mengirimkan data notification
		$data["error"]		= $this->session->flashdata("error");
		$data["success"]	= $this->session->flashdata("success");



		// load data model yang ada libraray model
		$data['data_buku']			= $this->home_model->readBuku();
		$data['data_distributor']	= $this->home_model->readDistributor();
		$data['data_pasok']			= $this->home_model->readPasok();
		$data['read_distributor']	= $this->home_model->readDistributor("id_distributor LIMIT 5");
		$data['read_kasir']			= $this->home_model->readKasir("id_kasir LIMIT 5");
		$data['data_kasir']			= $this->home_model->readKasir();
		$data['view']	= "home/v_home";
		$this->load->view("index",$data);

	}
}
